# Behavior Designer

[Behavior Designer - Behavior Trees for Everyone | 可视化脚本 | Unity Asset Store](https://assetstore.unity.com/packages/tools/visual-scripting/behavior-designer-behavior-trees-for-everyone-15277)

## 如何使用

强烈建议直接下载 zip 后解压到项目中，虽然可以以包的形式添加，但是无法打开设置界面等

1. 复制 git 地址：`https://gitlab.com/hbfpt/Unity/Plugin/behavior-designer.git`
2. 打开工程，点击窗口->包管理器->加号->添加来自 git URL 的包

![image.png](https://s2.loli.net/2023/04/08/b1rn9XVG6diJq7v.png)

3. 输入复制的 git 地址
4. 等待下载完成即可！
